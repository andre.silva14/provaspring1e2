package br.com.proway.provaspring1e2;

import br.com.proway.provaspring1e2.model.Dados;
import br.com.proway.provaspring1e2.model.Livros;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        int codigo = 1;
        String texto;
        Dados descricao = new Dados();

        while (true){
            texto = "         Biblioteca \n";

            for (int i=0; i < descricao.Biblioteca.size(); i++){
                texto += descricao.Biblioteca.get(i).codigo + "  Título: "+ descricao.Biblioteca.get(i).titulo + "  Autor: "+
                        descricao.Biblioteca.get(i).autor + "  Avaliação: "+ descricao.Biblioteca.get(i).avaliacao + "\n";
            }
            String[] opcoes = {"Adicionar", "Deletar", "Sair"};
            int opcao = JOptionPane.showOptionDialog(null, texto, "Biblioteca",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opcoes, opcoes[0]);

            if (opcao == 0){
                String titulo = JOptionPane.showInputDialog("Digite o título:");
                String autor = JOptionPane.showInputDialog("Digite o nome do autor:");
                int avaliacao = Integer.parseInt(JOptionPane.showInputDialog("Avalie o livro de 1 à 5:"));
                descricao.inserirLivro(new Livros(codigo, titulo, autor, avaliacao));
                codigo++;
            } else if (opcao == 1) {
                int codigoDelete = Integer.parseInt(JOptionPane.showInputDialog("Digite o código do livro:"));
                descricao.deletarLivro(codigoDelete);
            }else if (opcao == -1 || opcao == 2){
                break;
            }

        }
    }
}
