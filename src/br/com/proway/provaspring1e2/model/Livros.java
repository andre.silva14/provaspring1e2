package br.com.proway.provaspring1e2.model;

public class Livros {
    public int  codigo;
    public String titulo;
    public int avaliacao;
    public String autor;

    public Livros(int id, String titulo, String autor, int codigo){
        this.codigo = id;
        this.titulo = titulo;
        this.autor = autor;
        this.avaliacao = codigo;

    }
}

