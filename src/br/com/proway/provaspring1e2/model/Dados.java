package br.com.proway.provaspring1e2.model;

import javax.swing.*;
import java.util.ArrayList;

public class Dados {
    public ArrayList<Livros> Biblioteca = new ArrayList<>();

    public void inserirLivro(Livros livros) {
        Biblioteca.add(livros);
    }

    public void deletarLivro(int codigo) {
        boolean deletou = Biblioteca.removeIf(l -> l.codigo == codigo);
       if (deletou == false){
           JOptionPane.showMessageDialog(null, "O código informado não existe");
       }
    }


}
